from django.urls import include, path
from django.conf.urls import url

from rest_framework import permissions, routers
from rest_framework_simplejwt import views as jwt

from . import views, serializers

router = routers.DefaultRouter()

urlpatterns = [
    path(str(), include(router.urls)),
    path('api/users', views.Users.as_view()),
    path('api/token', jwt.TokenObtainPairView.as_view(serializer_class=serializers.JWTSerializer)),
    path('api/token/refresh', jwt.TokenRefreshView.as_view())
]