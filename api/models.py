from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

@receiver(pre_save, sender=User)
def set_active_false(sender, instance, **kwargs):
    if instance.pk == None and instance.is_superuser == False:
        instance.is_active = False

@receiver(pre_save, sender=User)
def set_full_name(sender, instance, **kwargs):
    if instance.pk == None and instance.is_superuser == False:
        instance.first_name = instance.first_name.title()
        instance.last_name = instance.last_name.title()

class User(AbstractUser):
    pass