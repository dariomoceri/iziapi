from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError, PermissionDenied

from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from api.models import *

import re

class JWTSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        credentials = { 'username': None, 'password': attrs.get('password') }

        identifier = attrs.get('username')

        user = User.objects.filter(email=identifier).first() or User.objects.filter(username=identifier).first()

        if user is None or user.is_active == False: raise PermissionDenied()
            
        credentials['username'] = user.username

        return super().validate(credentials)

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def validate_email(self, value: str) -> str:
        email = value.lower()

        if User.objects.filter(email=email).first():
            raise ValidationError('Un utente con questa email è già presente.')

        return email

    def validate_username(self, value: str) -> str:
        username = value.lower()

        pattern = r'^[a-zA-Z0-9_]*$'

        if re.match(pattern, username) == None:
            raise ValidationError('Lo username non può contenere nessun carattere speciale.')

        if User.objects.filter(username=username).first():
            raise ValidationError('Un utente con questo nome è già presente.')

        return username

    def validate_password(self, value: str) -> str:
        validate_password(value); hash = make_password(value)
        return hash

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'password', 'is_active')