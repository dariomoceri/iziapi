from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.core.exceptions import ValidationError
from django.conf import settings

def email(subject: str, recipient: list, template: str, content: dict):
    content = get_template(template).render(content)

    msg = EmailMessage(subject, content, f'"IZI - Store" <{settings.EMAIL_ADDRESS}>', recipient)

    msg.content_subtype = 'html'

    msg.send()

def safe_save(model: object):
    try:
        model.save()
    except ValidationError as error:
        return error