from django.core.exceptions import ValidationError

def not_empty_validator(value: str):
    length, space = len(value), value.isspace()

    if length == 0 or space:
            raise ValidationError('Il contenuto di questo campo non può essere vuoto o formato solo da spazi.')