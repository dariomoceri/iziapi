# IZI - API Endpoints
### **POST** `/api/token` :
* <u>Request:</u> 
	 * `username`: Username o e-mail dell'utente per cui si vuole generare il token.
	 * `password`: La password in chiaro dell'utente.
* <u>Response:</u>  
	 * `access`: Il token d'accesso da inserire nell'Header HTTP `Authorization` con Champion `Bearer`, dura 15 minuti.
	 * `refresh`: Il token di refresh per ottenere un nuovo access, dura 30 giorni e si utilizza solo a quello scopo.
	 
### **POST** `/api/token/refresh` :
* <u>Request:</u> 
	 * `refresh`: Un refresh-token valido.
* <u>Response:</u>  
	 * `access`: Il nuovo access-token generato per l'utente a cui appartiene il token di refresh utilizzato.