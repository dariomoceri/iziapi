from django.core.exceptions import ValidationError

import re

class NumberValidator(object):
    error = 'La password deve contenere almeno un carattere numerico, 0-9.'

    def validate(self, password, user=None):
        if not re.findall('\d', password):
            raise ValidationError(self.error)

    def get_help_text(self):
        return self.error

class UppercaseValidator(object):
    error = 'La password deve contenere almeno una lettera maiuscola, A-Z.'

    def validate(self, password, user=None):
        if not re.findall('[A-Z]', password):
            raise ValidationError(self.error)

    def get_help_text(self):
        return self.error

class LowercaseValidator(object):
    error = 'La password deve contenere almeno una lettera minuscola, a-z.'

    def validate(self, password, user=None):
        if not re.findall('[a-z]', password):
            raise ValidationError(self.error)

    def get_help_text(self):
        return self.error
